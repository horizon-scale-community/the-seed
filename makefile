
#  go user service

before-dockerise-go-user-service: tidy-user-service vendor-user-service build-proto-user-service

tidy-user-service:
	cd services/go/user-service/; go mod tidy

vendor-user-service:
	cd services/go/user-service/; go mod vendor

build-proto-user-service:
	protoc --go_out=plugins=grpc:. .proto/v1/*.proto

build-user-service:
	cd services/go/user-service/; docker-compose up --build
