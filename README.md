# The Seed

## Services

[user-service](services/go/user-service/README.md)

## Tech

- C# ( .net6)

- Vue3

- Golang

- Grpc

- Nats

- AuthO2


## How to participate ?

 Firstly , download & install [Git](https://git-scm.com/) on your machine. Then fetch and **pull --rebase** the repo and you're ready to go.

## How can I use your tools ? 

### make

-> on windows : ```choco install make``` or install nMake

-> on ubuntu : 
``` 
    sudo apt update
    sudo apt install build-essential
```

## How we organise our git branches ?

We have only 2 main branches :

- Master ==> for the final product

- dev ==> for "merge" all the instant team work

Then whe have the features branches :

Simply, after discuss, we open an issue on gitlab and create an associate merge request. When our job is done we change the merge-request status to review and move the issue into the review column of the kanban table.


**PLEASE USE REBASE INSTEAD OF MERGE**

