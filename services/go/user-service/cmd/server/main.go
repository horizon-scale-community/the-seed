package main

import (
	"log"

	"gitlab.com/horizon-scale-community/the-seed/user-service/internal/domain"
	"gitlab.com/horizon-scale-community/the-seed/user-service/internal/handlers"
	"gitlab.com/horizon-scale-community/the-seed/user-service/services"
)

func Run() error {
	//Wiring
	userRepository, err := domain.NewUserRepository()
	if err != nil {
		return err
	}
	userGRPCHandler := handlers.NewUserGRPCHandler(services.NewUserService(&userRepository))
	if err := userGRPCHandler.Serve(); err != nil {
		return err
	}

	return nil
}

func main() {
	if err := Run(); err != nil {
		log.Fatal(err)
	}
}
