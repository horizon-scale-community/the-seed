package handlers

import (
	"context"
	"log"
	"net"

	"gitlab.com/horizon-scale-community/the-seed/user-service/internal/domain"
	usr "gitlab.com/horizon-scale-community/the-seed/user-service/internal/proto"
	"gitlab.com/horizon-scale-community/the-seed/user-service/services"
	"google.golang.org/grpc"
)


// UserGRPCHandler -- will handle incoming gRPC Requests
type UserGRPCHandler struct {
	UserService services.UserService
}

// New -- returns a new gRPC UserGRPCHandler
func NewUserGRPCHandler(userService services.UserService) *UserGRPCHandler {
	return &UserGRPCHandler{
		UserService: userService,
	}
}

func (h *UserGRPCHandler) Serve() error {
	lis, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Print("could not listen on port 50051")
		return err
	}

	grpcServer := grpc.NewServer()
	usr.RegisterUserServiceServer(grpcServer, h)

	if err := grpcServer.Serve(lis); err != nil {
		log.Printf("failed to serve: %s\n", err)
	}

	return nil
}

func (h *UserGRPCHandler) GetUser(ctx context.Context, req *usr.GetUserRequest) (*usr.GetUserResponse, error) {
	log.Print("Get user Endpoint hit")
	user, err := h.UserService.GetUserByID(ctx, req.Id)

	if err != nil {

		return &usr.GetUserResponse{}, err
	}
	return &usr.GetUserResponse{
		User: &usr.User{
			ID:        user.ID,
			Name:      user.Name,
			FirstName: user.FirstName,
		},
	}, nil
}

func (h *UserGRPCHandler) CreateUser(ctx context.Context, req *usr.CreateUserRequest) (*usr.CreateUserResponse, error) {
	log.Print("Create Rocket gRPC endpoint hit")
	newUsr, err := h.UserService.CreateUser(ctx, domain.User{
		FirstName: req.User.FirstName,
		Name:      req.User.Name,
	})

	if err != nil {
		log.Print("failed to insert rocket into database")
		return &usr.CreateUserResponse{}, err
	}

	return &usr.CreateUserResponse{
		User: &usr.User{
			ID:        newUsr.ID,
			Name:      newUsr.Name,
			FirstName: newUsr.FirstName,
		},
	}, nil
}

func (h *UserGRPCHandler) UpdateUser(ctx context.Context, req *usr.UpdateUserRequest) (*usr.UpdateUserResponse, error) {
	log.Print("Update Rocket gRPC endpoint hit")
	upUsr, err := h.UserService.UpdateUser(ctx, domain.User{
		ID:        req.User.ID,
		FirstName: req.User.FirstName,
		Name:      req.User.Name,
	})

	if err != nil {
		log.Print("failed to insert rocket into database")
		return &usr.UpdateUserResponse{}, err
	}

	return &usr.UpdateUserResponse{
		User: &usr.User{
			ID:        uint64(upUsr.ID),
			Name:      upUsr.Name,
			FirstName: upUsr.FirstName,
		},
	}, nil

}

func (h *UserGRPCHandler) DeleteUser(ctx context.Context, req *usr.DeleteUserRequest) (*usr.DeleteUserResponse, error) {
	log.Print("delete rocket gRPC endpoint hit")

	err := h.UserService.DeleteUser(ctx, req.Id)

	if err != nil {
		return &usr.DeleteUserResponse{}, err
	}

	return &usr.DeleteUserResponse{
		Status: "successfully delete user",
	}, nil
}
