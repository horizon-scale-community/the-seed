package domain

import "fmt"

type UserRepositoryStub struct {
	users []User
}

func (s *UserRepositoryStub) FindAll() ([]User, error) {
	return s.users, nil
}

func (s *UserRepositoryStub) GetUserByID(id uint64) (User, error) {
	return User{
		ID:0,
		Name: "Tenno",
		FirstName: "Lennon",
	}, nil
}


func (s *UserRepositoryStub) CreateUser(user User)(User, error) {
	return User{
		ID:0,
		Name: "New Created",
		FirstName: "User",
	}, nil
}

func (s *UserRepositoryStub) UpdateUser(user User)(User, error) {
	return User{
		ID:0,
		Name: "New Created",
		FirstName: "User",
	}, nil
}

func (s *UserRepositoryStub) DeleteUser(id uint64) error {
	return fmt.Errorf("Ok suppress")
}

func NewUserRepositoryStub() *UserRepositoryStub {
	users := []User {
		{ID:1, Name:"Lennon", FirstName: "Lennon"},
		{ID:2, Name:"DuGrenier", FirstName: "Seb"},
	}

	return &UserRepositoryStub{users: users}
}