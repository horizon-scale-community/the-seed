package domain

import (
	"fmt"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// DB
type Repository struct {
	db *gorm.DB
}

// New - returns a new Repository
func NewUserRepository() (Repository, error) {

	dbUsername := os.Getenv("DB_USERNAME")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbHost := os.Getenv("DB_HOST")
	dbTable := os.Getenv("DB_TABLE")
	dbPort := os.Getenv("DB_PORT")

	connectString := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable", dbHost, dbPort, dbUsername, dbTable, dbPassword)

	db, err := gorm.Open(postgres.Open(connectString), &gorm.Config{})
	if err != nil {
		return Repository{}, err
	}

	// Migrate the schema
	db.AutoMigrate(&User{})

	db.Create(&User{
		Name:      "RIGAUD FLEJOU DE LA VARENNE DE VILLEMONTE",
		FirstName: "Louis",
	})

	return Repository{
		db: db,
	}, nil
}

// GetUserByID -- returns a user from the database by a given ID
func (s *Repository) GetUserByID(id uint64) (User, error) {
	var user User

	if err := s.db.First(&user, id).Error; err != nil {
		return user, err
	}
	return user, nil
}

// CreateUser -- create a new user into the database
func (s *Repository) CreateUser(usr User) (User, error) {
	user := User{
		Name:      usr.Name,
		FirstName: usr.FirstName,
	}

	if err := s.db.Create(&user).Error; err != nil {
		return user, err
	}

	return user, nil
}

// UpdateUser -- update the repertored user's data
func (s *Repository) UpdateUser(usr User) (User, error) {
	var updateUser User
	s.db.First(&updateUser, usr.ID)
	updateUser = usr
	if err := s.db.Save(&updateUser).Error; err != nil {
		return User{}, err
	}
	return updateUser, nil
}

// DeleteRocket - deletes a user from the database by it's ID
func (s *Repository) DeleteUser(id uint64) error {
	var deleteUser User
	if err := s.db.Delete(&deleteUser, id).Error; err!= nil {
		return err
	}
	return nil
}
