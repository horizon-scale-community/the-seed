package services

import (
	"context"

	"gitlab.com/horizon-scale-community/the-seed/user-service/internal/domain"
)

type UserService interface {
	GetUserByID(ctx context.Context, id uint64) (domain.User, error)
	CreateUser(ctx context.Context, user domain.User) (domain.User, error)
	UpdateUser(ctx context.Context, user domain.User) (domain.User, error)
	DeleteUser(ctx context.Context, id uint64) error
}


type DefaultUserService struct {
	repo domain.UserRepository
}



func NewUserService(repository domain.UserRepository) *DefaultUserService {
	return &DefaultUserService{
		repo: repository,
	}
}


// User service functions :

func (s *DefaultUserService) GetUserByID(ctx context.Context, id uint64) (domain.User, error) {
	user, err := s.repo.GetUserByID(id)
	if err != nil {
		return domain.User{}, err
	}

	return user, nil
}

func (s *DefaultUserService) CreateUser(ctx context.Context, user domain.User) (domain.User, error) {
	user, err := s.repo.CreateUser(user)
	if err != nil {
		return domain.User{}, err
	}
	return user, nil
}

func (s *DefaultUserService) UpdateUser(ctx context.Context, user domain.User) (domain.User, error) {
	user, err := s.repo.UpdateUser(user)
	if err != nil {
		return domain.User{}, err
	}
	return user, nil
}

func (s *DefaultUserService) DeleteUser(ctx context.Context, id uint64) error {
	err := s.repo.DeleteUser(id)
	if err != nil {
		return err
	}
	return nil
}
